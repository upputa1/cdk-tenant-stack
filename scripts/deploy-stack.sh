rm -rf layer/
mkdir -p layer/nodejs
cp package*.json layer/nodejs/

# Install lambda dependencies into the layer folder.
# Note: node-modules needs to under folder named "nodejs" for lambda layer to work
npm install --production --prefix layer/nodejs/
npm install --production --prefix deploy/nodejs/

# package-lock.json changes for each npm install, which signals new version of Lambda layer.
# Delete this, so that Lambda layer version doesnt changes unless there is an update to packages
rm -rf layer/nodejs/package-lock.json
rm -rf deploy/nodejs/package-lock.json

cdk deploy --require-approval=never
