#!/usr/bin/env bash

# Use this script if you need the entire environment set up
# Though it is not be harmful to run this regardless
# what state your environment is in.

scriptDir="$(dirname "$0")"

. $scriptDir/local/_util.sh

. $scriptDir/local/nuke.sh

info "Performing environment setup"

. $scriptDir/local/initial-setup.sh

info "Installing app\n"

. $scriptDir/local/app-setup.sh

success "Congrats, you are all set up!\n"

success "If you have switched node.js versions, you may need to open a new tab in your terminal to have that change take place"