/* Copyright (c) 2021 RSA Security LLC or its affiliates. All rights reserved. */

const { App } = require('@aws-cdk/core');
const hello = require('./HelloStack');
const app = new App();
const tenantId = app.node.tryGetContext('tenantId');
console.log('tenantId: ${tenantId}');
const stack = new hello.HelloCdkStack(app, `Spike-cdk1-${tenantId}`, {
  tenantId
});


app.synth();
