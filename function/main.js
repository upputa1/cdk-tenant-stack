/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */


const exec = require('child_process').exec;

const handler = (event, context, callback) => {
  console.log(event);
  console.log(event.tenantId);

  const cdk = '/opt/nodejs/node_modules/aws-cdk/bin/cdk';
  const params = `tenantId=${event.tenantId}`;
  console.log(params);
  const deploy = `deploy --debug --require-approval never --no-staging --app HelloApp.js -c ${params}  --ec2creds false --path-metadata false --asset-metadata false --all -o /tmp/cdk.out`;
  const CMD = `${cdk} ${deploy}`;
  console.log('inside main handler', CMD);

  const child = exec(CMD, (error) => {
    // Resolve with result of process
    callback(error, 'Process complete!');
  });

  // Log process stdout and stderr
  child.stdout.on('data', console.log);
};

module.exports = {
  handler
};
