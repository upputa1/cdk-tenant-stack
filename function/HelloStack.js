/* Copyright (c) 2021 RSA Security LLC or its affiliates. All rights reserved. */


const lambda = require('@aws-cdk/aws-lambda');
const iam = require('@aws-cdk/aws-iam');
const cdk = require('@aws-cdk/core');
const ssm = require('@aws-cdk/aws-ssm');
const appSync = require('@aws-cdk/aws-appsync');
const hello2 = require('./HelloStack2');

class HelloCdkStack extends cdk.Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const env = ssm.StringParameter.valueForStringParameter(this, '/NWS/ops/env');
    console.log('env value', env);
    console.log(props);

    const stack2 = new hello2.HelloCdkStack2(this, `Spike-cdk2-${props.tenantId}`, {
      props
    });

    const lambdaRole = new iam.Role(this, `spike-lambda-role-${props.tenantId}`, {
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
      roleName: `Spike-test--${props.tenantId}-Lambda-Role`,
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName(
          'service-role/AWSLambdaBasicExecutionRole')
      ]
    });

    const spikeLambda = new lambda.Function(this, `spike-lambda-${props.tenantId}`, {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset('.'),
      handler: 'tenant-lambda.handler',
      role: lambdaRole,
      functionName: `Spike-test-${props.tenantId}-lambda`,
      layers: [ lambda.LayerVersion.fromLayerVersionArn(this, 'app-layer- 1', 'arn:aws:lambda:us-east-1:642374541031:layer:NWS-anu-admin-cdk-dependenices-layer:5')],
      timeout: cdk.Duration.seconds(5)
    });

    const appSync2EventBridgeGraphQLApi = new appSync.CfnGraphQLApi(
      this,
      'AppSync2EventBridgeApi',
      {
        name: 'AppSync2EventBridge--${props.tenantId}-API',
        authenticationType: 'API_KEY'
      }
    );
    new appSync.CfnApiKey(this, 'AppSync2EventBridgeApiKey-', {
      apiId: appSync2EventBridgeGraphQLApi.attrApiId
    });

    const apiSchema = new appSync.CfnGraphQLSchema(this, 'ItemsSchema', {
      apiId: appSync2EventBridgeGraphQLApi.attrApiId,
      definition: `type Event {
        result: String
      }
      
      type Mutation {
        putEvent(event: String!): Event
      }
      
      type Query {
        getEvent: Event
      }
      
      schema {
        query: Query
        mutation: Mutation
      }`
    });
  }

}

module.exports = { HelloCdkStack };