/* Copyright (c) 2021 RSA Security LLC or its affiliates. All rights reserved. */



const cdk = require('@aws-cdk/core');
const s3 = require('@aws-cdk/aws-s3');
const ssm = require('@aws-cdk/aws-ssm');

class HelloCdkStack2 extends cdk.Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const env = ssm.StringParameter.valueForStringParameter(this, '/NWS/ops/env');
    console.log('env value', env);
    console.log(props);
    const envCondition = new cdk.CfnCondition(this, 'IsEnvProd', {
      expression: cdk.Fn.conditionOr(cdk.Fn.conditionEquals(env, 'prod'), cdk.Fn.conditionEquals(env, 'staging'))
    });

    const tenantBucket = new s3.Bucket(this, 'MyFirstBucket-${props.tenantId}', {
      versioned: true,
      bucketName: `cdk-bucket-${props.tenantId}`
    });

    const infraBucket = new s3.Bucket(this, 'InfraBucket-${props.tenantId}', {
      versioned: true,
      bucketName: `cdk-infra-bucket-${props.tenantId}`
    });
    const cfnBucket = tenantBucket.node.defaultChild;
    cfnBucket.cfnOptions.condition = envCondition;
  }

}

module.exports = { HelloCdkStack2 };