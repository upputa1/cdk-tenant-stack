/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

exports.handler = async({ arguments: args }) => {
  console.log('Message provided:', args.msg);
  const helloWorld = async() => `hello world${args.msg}`;
  const result = await helloWorld();
  console.log(result);
  return result;
};
