# cdk-tenant-stack

# Local Setup

`./scripts/welcome.sh`

This will get libraries installed and get the right version of node.js set up. If you are changing node versions, you may need to open a new terminal window to have the node version change take effect.

# Tests

## Unit Tests

`npm test`

## Integration Tests

`npm run it`

# Stack Deployment

`deploy`, `undeploy` commands deploy, undeploy resources respectively with the prefix `NWS-${user}` where `${user}` is derived from AWS default profile user.

## Deploy

`npm run deploy`

## Undeploy

`npm run undeploy`

# Developing Locally with SAM

AWS SAM local can be used for local testing while developing.

Pre-requisites:

- Docker
- AWS CDK
- AWS SAM Cli

#### Steps

1. Deploy your lambda using `npm run deploy`. This is required for projects using lambda layer.
   `sam local` command downloads this layer from your default environment for any lambda invocations.

2. Generate the template using:

   `npm run synth`

3. To test any changes you have made locally, invoke the lambda

   `sam local invoke -t cdk.out/${NWS}-CdkTenantStackStack.template.json --event test/test.json
