# Versioning

This library uses [semantic-release](https://github.com/semantic-release/semantic-release) to automatically version, tag and publish the library when code merges.

# Commit Messages

This library uses commitlint to lint commit messages to ensure the comply to the [Angular configuration](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-angular). It is the forcing of this convention that enables semantic-release to properly manage releases.
