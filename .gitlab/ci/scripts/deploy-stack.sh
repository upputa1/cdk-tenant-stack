#!/usr/bin/env bash
npm init -y
echo @rsa-security:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
echo //gitlab.com/api/v4/packages/npm/:_authToken=${NPM_TOKEN} >>.npmrc
npm install @rsa-security/cdk-tenant-stack@$1
cd node_modules/@rsa-security/cdk-tenant-stack
npm install
npm run deploy

