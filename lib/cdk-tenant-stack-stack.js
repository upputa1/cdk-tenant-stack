/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import devTools from '@rsa-security/node-dev-tools';
import * as iam from '@aws-cdk/aws-iam';

const {
  dependenciesLayer,
  storeArnParameter,
  runtime
} = devTools.cdk.lambda;

const NAME = 'CdkTenantStack';

export class CdkTenantStackStack extends cdk.Stack {
  constructor(scope, id, props) {
    super(scope, id, props);

    const namespace = props.namespace;
    const service = props.service;
    const region = props.region;

    const lambdaRole = new iam.Role(this, 'TenantCdkaRole', {
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
      roleName: `${namespace}-${NAME}-${region}-Lambda-Role`,
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName(
          'service-role/AWSLambdaBasicExecutionRole'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AWSCloudFormationFullAccess'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AWSAppSyncAdministrator'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMFullAccess'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonS3FullAccess'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('IAMFullAccess'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AWSLambdaFullAccess')
      ]
    });
    const lambdaDependenciesLayer = dependenciesLayer(this, NAME, namespace, service);

    const cdkDependenciesLayer = new lambda.LayerVersion(this, 'cdk-dependenices-layer', {
      code: lambda.Code.fromAsset('deploy/'),
      compatibleRuntimes: [lambda.Runtime.NODEJS_14_X],
      description: 'NWS SDK layer dependencies',
      layerVersionName: `${namespace}-${service}-cdk-dependenices-layer`
    });

    const aLambda = new lambda.Function(this, `${NAME}Handler`, {
      runtime,
      code: lambda.Code.fromAsset('function'),
      handler: 'main.handler',
      role: lambdaRole,
      functionName: `${namespace}-${service}-${NAME}Lambda`,
      layers: [lambdaDependenciesLayer, cdkDependenciesLayer],
      environment: {
        ENVIRONMENT: props.environment
      },
      timeout: cdk.Duration.seconds(30)
    });
    storeArnParameter(this, NAME, namespace, service, aLambda);
  }
}
