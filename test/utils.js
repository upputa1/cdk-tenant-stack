/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

import { readFileSync } from 'fs';
import path from 'path';

export const readFile = (fileName) => {
  const dataPath = path.resolve(__dirname, 'events', fileName);
  const eventFile = readFileSync(dataPath);
  const event = JSON.parse(eventFile);
  return event;
};
