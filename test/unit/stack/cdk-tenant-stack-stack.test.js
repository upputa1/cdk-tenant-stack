/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

const { expect, countResources, haveResource } = require('@aws-cdk/assert');
const cdk = require('@aws-cdk/core');
const fs = require('fs');
const path = require('path');

const CdkTenantStack = require('../../../lib/cdk-tenant-stack-stack');

const pathToLayer = path.join(__dirname, '..', '..', '..', 'layer');

beforeAll(function() {
  // need to get layer directory in place for CDK to build
  fs.mkdirSync(pathToLayer);
});

afterAll(function() {
  fs.rmdirSync(pathToLayer);
});

describe('CdkTenantStack Stack', () => {
  it('Stack with one function', () => {
    const app = new cdk.App();
    const stack = new CdkTenantStack.CdkTenantStackStack(app, 'MyTestStack', {
      namespace: 'NWS-test',
      service: 'foo-service'
    });

    expect(stack).to(countResources('AWS::Lambda::Function', 1));
    expect(stack).to(countResources('AWS::Lambda::LayerVersion', 1));
    expect(stack).to(countResources('AWS::IAM::Role', 1));
    expect(stack).to(countResources('AWS::SSM::Parameter', 1));

    expect(stack).to(haveResource('AWS::SSM::Parameter', {
      AllowedPattern: '.*',
      Description: 'CdkTenantStack Lambda',
      Name: '/NWS-test/foo-service/CdkTenantStack-lambda-arn',
      Type: 'String'
    }));

  });
});
