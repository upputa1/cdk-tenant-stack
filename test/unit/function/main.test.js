/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

import { handler } from '../../../function/main';
import { expect } from 'chai';

describe('Hello world', () => {
  test('will echo message', async() => {
    const result = await handler({
      arguments: {
        msg: ', dude'
      }
    });
    expect(result, 'function called and returned correct value').to.equal('hello world, dude');
  });
});
