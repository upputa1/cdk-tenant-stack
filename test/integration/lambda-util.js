/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

const AWS = require('aws-sdk');
const cdkConfig = require('../../cdk.json');
const devTools = require('@rsa-security/node-dev-tools');
const cdkUtils = devTools.cdk.utils;

AWS.config.update({ region: 'us-east-1' });

const lambda = new AWS.Lambda();

const invokeLambda = async(payload) => {
  try {

    // look into using dev-tools for this
    const service = cdkConfig.context.service;
    const namespace = await cdkUtils.getNameSpace();

    const lambdaParams = {
      FunctionName: `${namespace}-${service}-CdkTenantStackLambda`,
      Payload: JSON.stringify(payload)
    };
    const result = await lambda.invoke(lambdaParams).promise();
    return result;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

module.exports = {
  invokeLambda
};