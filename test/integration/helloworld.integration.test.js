/* Copyright (c) 2020-2021 RSA Security LLC or its affiliates. All rights reserved. */

const AWS = require('aws-sdk');
import { invokeLambda } from './lambda-util';
import { expect } from 'chai';
AWS.config.update({ region: 'us-east-1' });

describe('CdkTenantStack Lambda', () => {
  test('fetches hello world', async() => {
    const result = await fetchHelloWorld(', foooooo');
    expect(result.StatusCode, 'Lambda invoke is unsuccessful').to.equal(200);
    expect(JSON.parse(result.Payload), 'Fetches hello world').to.equal('hello world, foooooo');
  });
});

const fetchHelloWorld = async(msg) => {
  const request = {
    arguments: {
      msg
    }
  };
  return invokeLambda(request);
};